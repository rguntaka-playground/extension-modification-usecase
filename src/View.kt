abstract class View {
    fun showMessage(message:String) = println(message)
}