/*
 Extension vs Modification
 Any is a Kotlin keyword which will extend the behavior without modifying/ disturbing the existing

 Here, Any is used for a function to extend a behavior across the views without making any change to each view
 */

fun main(args: Array<String>){
    val list = NoteListView()
    val detail  = NoteDetailView()
    val login = LoginView()

    list.showMessage("From List View")
    detail.showMessage("From Detail View")
    login.showMessage("From Login View")

    list.anotherShowMessage("Any: From List View")
    detail.anotherShowMessage("Any: From Detail View")
    login.anotherShowMessage("Any: From Login View")

}

//<Receiver>.functionName()
// This is an alternative to abstract class way of implementing showMessage()
// In Java, abstract class is the preferable way for this kind of use cases whereas in Kotlin extension functions are preferable
// Reason: easier to create & use
fun Any.anotherShowMessage(message:String) = println("From Any Message $message")